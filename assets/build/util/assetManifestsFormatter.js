const path = require('path');

module.exports = (key, value) => {
  if (typeof value === 'string') {
    return value;
  }
  const manifest = value;
  
  Object.keys(manifest).forEach((src) => {
    const sourcePath = path.basename(path.dirname(src));
    const targetPath = path.basename(path.dirname(manifest[src]));
    if (sourcePath === targetPath) {
      return;
    }
    manifest[`${targetPath}/${src}`] = manifest[src];
    delete manifest[src];
  });
  return manifest;
};
