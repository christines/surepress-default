<?php 
    use Surepress\Functions\Common as Common;
    use Surepress\Functions\Assets as Assets;
    use Surepress\Functions\BreadCrumb as BreadCrumb;
?>
<?php get_header(); ?>
    <div class="container">
        <article class="page-container">
            <header>
                <?php// echo BreadCrumb\breadCrumb(); ?>
                <h1><?php the_title(); ?></h1>
             </header> 

            <?php if ( have_posts() ) : ?>
                
                <?php while ( have_posts() ) : the_post(); ?>          
                
                    <?php if( is_front_page() || ( !Common\is_main() && is_front_page() ) ): ?>
                        <?php get_template_part('template-parts/static', 'homepage' ); ?>
					<?php elseif( is_page('sitemap') ): ?>
                        <?php get_template_part('template-parts/static/page', 'sitemap' ); ?> 
                    <?php else : ?>     
                        <?php the_content(); ?>
                    <?php endif; ?>    
                
                <?php endwhile; ?>

            <?php else : ?>
                get_template_part('template-parts/content', 'none');
            <?php endif; ?>

        </article>
    </div>
<?php get_footer();?>