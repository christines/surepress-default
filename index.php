<?php get_header(); ?>
    <?php
    if ( !is_search() && have_posts() && !is_404() ) {
        while ( have_posts() ) : the_post();
            get_template_part('template-parts/content', get_post_type());
        endwhile;

    } else if( is_search() ) {
        get_template_part('template-parts/content', 'search'); //Search result template

    } else {    
        get_template_part('template-parts/content', 'none');
    }
    ?>

<?php get_footer(); ?>
