<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;
use Surepress\Functions\Scripts as Scripts;
?>
<!DOCTYPE html>
<html>
    <!-- Google Tag Manager -->
    <?php if (!empty($wp_query->nap->microsite_gtm_ga)): ?>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $wp_query->nap->microsite_gtm_ga; ?>');</script>
    <?php endif; ?>
    <!-- End Google Tag Manager -->

    <head>
      <?php get_template_part('template-parts/partials/header', 'meta'); ?> 
      <?php wp_head(); ?>
      <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>

    <body <?php body_class(); ?>>       
        <?php echo Scripts\body_scripts(); ?>
        <a name="top" id="top"></a>
        <header id="header_main">
            
            <nav>
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapsing_header_nav" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="<?php echo get_site_url(); ?>"><img src="<?php echo Assets\asset_path('images/logo.png') ?>" /></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="collapsing_header_nav">                   
                  <?php 
                      wp_nav_menu(
                          array(
                              'menu' => 'Main Menu', 
                              'container' => null,
                              'menu_id' => 'nav_main'
                          )
                      ); 
                  ?>                   

                  <ul class="nav navbar-nav" id="find_a_dealer">
                    <li><a href="#" data-toggle="collapse" data-target="#dropdown_search_form">Find A Dealer</a></li>
                  </ul>

                  <div id="dropdown_search_form" class="collapse">
                    <form method="get" action="/locations" data-ip="<?php echo Common\userIP(); ?>" class="searchform">
                      <input type="text" name="ds" id="ds" placeholder="Enter Zip Code or City" />
                      <button>Search Dealer <i class="fa fa-search" aria-hidden="true"></i></button>
                      <a href="#" id="location_button" class="myLocation">Use My Location</a>
                    </form>                     
                  </div>                    

                </div><!-- /.navbar-collapse -->


            </nav>  

        </header>
        <?php get_template_part('template-parts/section/header', 'hero'); ?> 
        <main>
             