<?php

//GET/LOAD ALL FUNCTION FILES @ 'functions' DIRECTORY
$files = glob( get_template_directory().'/functions/' . "*.php");
foreach($files as $file): 
    require $file; 
endforeach;


/*****************************************/
/***************************************/

    //default WP theme setup
    add_action('after_setup_theme', 'Surepress\\Functions\\Common\\defaultWp');

    //generate post types
    add_action( 'init', 'Surepress\\Functions\\PostTypes\\generatePostTypes',10);  
    //post type taxonomies
    add_action( 'init', 'Surepress\\Functions\\Taxonomies\\generateTaxonomy', 1 ); //ALways run taxonomy earlier.... dont change 1  
        //custom meta fields for products [images/sort]
        add_action( 'products-category_add_form_fields', 'Surepress\\Functions\\ProductMeta\\productImage', 10, 2 ); 
        add_action( 'products-category_edit_form_fields', 'Surepress\\Functions\\ProductMeta\\productImage', 10, 5 );    
        add_action( 'created_products-category', 'Surepress\\Functions\\ProductMeta\\productImageSave', 10, 2 );
        add_action( 'edited_products-category', 'Surepress\\Functions\\ProductMeta\\productImageEdit', 10, 2 );

    //add title slug in body class
    add_filter( 'body_class', 'Surepress\\Functions\\BodyClass\\bodyClass', 109);
    
    //theme css-js
    add_action('wp_enqueue_scripts', 'Surepress\\Functions\\Assets\\theme_assets', 100);
    
    //Page titles
    add_filter( 'document_title_parts', 'Surepress\\Functions\\Titles\\title', 100, 2 );    
    
    //filter to remove hentry
    add_filter( 'post_class', 'Surepress\\Functions\\Hentry\\remove', 10, 2 );
    
    //scripts and gtm
    add_action('wp_head','Surepress\\Functions\\Scripts\\header_scripts', 99);
    add_action('wp_footer','Surepress\\Functions\\Scripts\\footer_scripts', 99);

    //custom variables for yoast
    add_action('wpseo_register_extra_replacements', 'Surepress\\Functions\\Yoast\\register_custom_yoast_variables');

    //add json ld
    add_action('wp_head', 'Surepress\\Functions\\Common\\json_ld');