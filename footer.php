<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Scripts as Scripts;
?>

		</main>

        <?php get_template_part('template-parts/section/footer', 'find-local-dealer'); ?> 

		<footer id="footer_main" class="footer_main">
            <div class="container">
		        
                <figure><a href="<?php echo get_site_url(); ?>"><img src=" <?php echo Assets\asset_path('images/logo-white.png') ?>" /></a></figure>

                <form method="get" action="/locations">
                    <h3>Carrier Factory Authorized Dealer</h3>
                    <label for="s">Search for local dealers</label>
                    <p class="input-group">
                      <input type="text" class="form-control" placeholder="Enter Zip Code or City" name="ds" id="ds">
                      <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                      </span>
                    </p>                         
                </form>

                <div class="clearfix"></div>
                <p>
                <?php

                    $args = array(
                      'menu'            => 'Footer Menu', 
                      'menu_id'         => 'nav_footer',
                      'container'       => false,
                      'echo'            => false,
                      'items_wrap'      => '%3$s',
                      'depth'           => 0,
                    );

                    echo strip_tags(wp_nav_menu( $args ), '<a>' );                    
                ?>
                </p>

                <p align="right">Sigler © 2018. All Rights Reserved</p>

            </div>  
		</footer>
        <a href="#top" id="back_to_top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
		<?php wp_footer(); ?>
	</body>
</html>