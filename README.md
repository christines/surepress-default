# Surepress Default #

### Requirements ###

* Node V8.12.0 or higher (node -v)
* NPM V6.4.1 or higher (npm -v)
* Yarn V1.6.0 or higher (yarn -v)


### Installation ###

* Create a virtual host:
```
    <VirtualHost *:443>
        
        DocumentRoot "PATH/OF_YOUR/LOCAL_FILES"
        ServerName www.testsite.com
        ServerAlias testsite.com

        SSLEngine on
        SSLCertificateFile "conf/ssl.crt/server.crt"
        SSLCertificateKeyFile "conf/ssl.key/server.key"
        
        <Directory "PATH/OF_YOUR/LOCAL_FILES">
            Allow from all
            Require all granted
            Options +FollowSymLinks
            Options +Indexes
        </Directory>
        
    </VirtualHost>
```

* Configure hosts file:
```
            -- 127.0.0.1 www.testsite.com testsite.com  
```

* Run command prompt at the theme root directory and run these commands to install dependancies:
```
            -- npm install
```

### Compiling Assets ###

* yarn build (for local development... UNMINIFIED)
* yarn build:production (for dev/master... MINIFIED)


### Watching Files for Local Development (yarn start) ###
This feature automatically compiles the css/js/images whenever you add/edit/delete/save files in the assets folder

* Create a file in surepress-sg/assets/config-local.json with the content:
```
          {
               "publicPath": "/wp-content/themes/surepress-default",
               "devUrl": "https://www.testsite.com"
          }
```
* run the command prompt at the theme root and type: yarn start    
* to stop watching files, press CTR+C in the command prompt
=======



