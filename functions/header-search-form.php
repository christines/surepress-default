<?php
namespace Surepress\Functions\SearchForm;

function header() {
    if( is_page( array('dealers', 'locations') ) ) {
        get_template_part('template-parts/section/header', 'hero-search-form');        
    }
}