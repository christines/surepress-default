<?php
namespace Surepress\Functions\Assets;
use Surepress\Functions\Microsites;

if ( ! function_exists('asset_path') ) :
    function asset_path($asset) {
        if ( file_exists( $manifest = get_stylesheet_directory() . '/dist/assets.json' ) ) {
            $manifest = json_decode(file_get_contents($manifest), true);
            return get_stylesheet_directory_uri() . '/dist/' . $manifest[$asset];
        } else {
        }

        return get_stylesheet_directory_uri() . '/dist/' . $asset;
    }
endif;

/**
 * Theme assets
 */
function theme_assets() {
    wp_enqueue_style('surepress-default-css-frontend', asset_path('styles/frontend.css'), false, null);
    wp_enqueue_script('surepress-default-js-frontend', asset_path('scripts/frontend.js'), '', null, true);

    /*if( is_page( array('dealers-search', 'locations') ) ){ 
          wp_enqueue_script('google-maps', '//maps.google.com/maps/api/js?key=AIzaSyDAZzkjfo5i1kHQUok7i7jsTsceTGlWyN8', ['jquery'], null, true);
    }    

    if( is_page( array('dealers', 'locations') ) ) { 
        wp_enqueue_script('surepress-ccc/api', asset_path('scripts/api.js'), ['google-maps'], null, true);        
        wp_enqueue_script( 'surepress-ccc/api' );
    } WHEN ADDING EXTERNAL JS*/     
    wp_enqueue_script( 'surepress-default/js' );
}