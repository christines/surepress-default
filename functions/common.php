<?php
namespace Surepress\Functions\Common;

function is_main(){
    if( get_current_blog_id() === 1){
        return true;
    }else{
        return false;
    }   
}


function defaultWp(){
    add_theme_support( 'menus' );
    add_theme_support( 'post-thumbnails' ); 
    add_theme_support( 'title-tag' );   

    add_image_size( '320x295', 320, 295, true ); //team archive thumb
    add_image_size('events-archive', 400, 600, true);
    add_image_size('products-archive', 400, 400, true);
    add_image_size('post-single', 900, 500, true);
    add_image_size('525x382', 525, 382, true);
    add_image_size('421x461', 421, 461, true); //before and after gallery
    add_image_size('1100x565', 1100, 565, true); //Service Page Single (Child pages section)
    add_image_size('282x122', 282, 122, true); //Service Page Single (Child pages section :: View Gallery)
    add_image_size('120x100', 120, 100, true); //Service Page Single (Child pages section :: Tab Navs)
    add_image_size('services-intro', 900, 600, true);
}


function formatPhone($phone){
    if($phone === "") return;
    $output = sprintf("(%s) %s-%s",
              substr($phone, 0, 3),
              substr($phone, 3, 3),
              substr($phone, 6, 4));
    return $output;
    //return $phone;
}

function userIP()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else
    {
      $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}


function json_ld(){

    //move this inside condition if no schema on main pages
    $schema = array(
        "@context"      => "http://schema.org",
        "@type"         => "HVACBusiness",
        "Name"          => "",
        "Description"   => "",
        "image"         => "",
        "priceRange"    => "Get a free quote",
        "Address"       => array(array(
                            "@type" => "PostalAddress",
                            "addressCountry"    => "United States",
                            "addressLocality"   => "",
                            "addressRegion" => "",
                            "postalCode"    => "",
                            "streetAddress" => "",
                            "telephone"     => ""
                        )),
         "areaServed" => array(
                             "@type" => "Place",
                             "containsPlace" => array(
                                "@type" => "Place",
                                "address"   => array(
                                    "@type" => "PostalAddress",
                                    "addressCountry"   => "United States",
                                    "addressRegion" => "",
                                    "addressLocality" => array()
                                    ),
                            ),
                        ),
          "sameAs" => "",
          "openingHours" => array()
      );


    if( get_post_type() == 'dealers'){
        $dealer = get_post_meta( get_the_ID() );
        $social = ( isset($dealer['socialmedia']) ) ? unserialize ($dealer['socialmedia'][0]) : null;
        $operation = ( isset($dealer['hours_operation']) ) ? unserialize ($dealer['hours_operation'][0]) : null;
        $operation_hours = [];

        foreach($operation as $o=>$x){
              array_push($operation_hours,ucwords(substr($o, 0, 2)).' '.$x['opening']['hour'].':'.$x['opening']['minute'].'-'.$x['closing']['hour'].':'.$x['closing']['minute']);
        }

        $schema["Name"] = $dealer["company"][0];
        $schema["Description"] = $dealer["business_description"][0]; 
        $schema["image"] = isset($dealer['_thumbnail_id']) ?  wp_get_attachment_url( $dealer['_thumbnail_id'][0] ) : "";
        $schema["Address"][0]["addressLocality"] = $dealer["dealer_city"][0];
        $schema["Address"][0]["addressRegion"] = $dealer["state"][0];
        $schema["Address"][0]["postalCode"] = $dealer["zip"][0];
        $schema["Address"][0]["streetAddress"] = $dealer["dealeraddress"][0];
        $schema["Address"][0]["telephone"] = $dealer["phone"][0];
        $schema["areaServed"]["containsPlace"]["address"]["addressRegion"] = $dealer["state"][0];
        $schema["areaServed"]["containsPlace"]["address"]["addressLocality"] = array( $dealer["dealeraddress"][0] );
        $schema["sameAs"] = array( isset($social[0]["url"]) ? $social[0]["url"] : ""  ); 
        $schema["openingHours"] = $operation_hours; 

         echo '<script type="application/ld+json">' . json_encode($schema) . '</script>';
    }
     
   
}