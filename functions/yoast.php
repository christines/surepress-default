<?php
namespace Surepress\Functions\Yoast;


function get_dealer_name(){
	$dealer = get_post_meta( get_the_ID() );
	return ucwords(strtolower($dealer['company'][0]));
}

function get_dealer_city(){
	$dealer = get_post_meta( get_the_ID() );
    return ucwords(strtolower($dealer['dealer_city'][0]));
}

function get_dealer_state(){
	$dealer = get_post_meta( get_the_ID() );
	return $dealer['state'][0];
}

function get_dealer_phone_number(){
	$dealer = get_post_meta( get_the_ID() );
	return $dealer['phone'][0];
}

function get_dealer_service_area(){
	$dealer = get_post_meta( get_the_ID() );
	return ucwords(strtolower($dealer['service_areas'][0]));
}

/**
 * Dealers - custom yoast variables
 */


function register_custom_yoast_variables(){
	wpseo_register_var_replacement('dealer_name', __NAMESPACE__. '\\get_dealer_name', 'advanced', 'dealer name');
	wpseo_register_var_replacement('dealer_city',  __NAMESPACE__. '\\get_dealer_city', 'advanced', 'dealer city');
	wpseo_register_var_replacement('dealer_state', __NAMESPACE__. '\\get_dealer_state', 'advanced', 'dealer state');
	wpseo_register_var_replacement('dealer_phone', __NAMESPACE__. '\\get_dealer_phone_number', 'advanced', 'dealer phone number');
	wpseo_register_var_replacement('dealer_service_area', __NAMESPACE__. '\\get_dealer_service_area', 'advanced', 'dealer service area');
}