<?php
namespace Surepress\Functions\BodyClass;
use Surepress\Functions\Common as Common; 

function bodyClass( $classes ) {
    global $post;
  
    if(!$post){
        return $classes;
    }

    if( !Common\is_main() && is_front_page() ){
        $classes[] = "microsite_home";
    }


    $classes[] = $post->post_name;

    $classes[] =  Common\is_main() ? "main" : "microsite";

    return $classes;
}

