<?php
namespace Surepress\Functions\BreadCrumb;

function breadCrumb() {

    if( is_page( array('dealers', 'dealers-single', 'dealers-search', 'locations') ) ) return; //disable breadcrumbs

    $output = null; 
    $output .= '<ol class="breadcrumb">';
    $output .= '<li><a href="'.home_url().'"><i class="fa fa-home" aria-hidden="true"></i></a></li>';
    $output .= " / ";    
    if (is_category() || is_single()) {
        $output .= "<li>fix this japol</li>";
    } elseif (is_page()) {
        $output .=  get_the_title();
    } elseif (is_search()) {
        $output .= "<li>fix this japol</li>";
    }

    $output .= '</ol>';

    return $output; //enable breadcrumbs    
    
}