<?php
namespace Surepress\Functions\Titles;

/**
 * OVERRIDE PAGE TITLES HERE!!!!!!!!!!!!!!!!!!!!!
 */
function title($title_parts) {

  /*sample below
  if (is_home() || is_front_page() ) {
    $title_parts['title'] = "Gian TAE TAE";
  } else {
    $title_parts['title'] = "Nich TAE TAE";
  }
  */
  return $title_parts;
}


function pageContentTitle() {
    $output = null;
    if ( is_page('locations') ) {
        if( isset($_GET['ds']) ) {
          $output = 'SHOWING <span id="result_count">results on your search</span>';  
        }
        
    } else if( is_page('dealers-single') ) {
        return;
    } else {
        $output = get_the_title();        
    }

    return $output;
}