<?php
namespace Surepress\Functions\Hentry;

function remove( $class ) {
    $class = array_diff( $class, array( 'hentry' ) );   
    return $class;
}