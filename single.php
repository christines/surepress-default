<?php get_header(); ?>
<div class="container">
    <div class="page-container">
        <?php
        if ( !is_search() && have_posts() && !is_404() ) {
            while ( have_posts() ) : the_post();
                get_template_part('template-parts/content', get_post_type());
            endwhile;

        } else if( is_search() ) {
            get_template_part('template-parts/content', 'search'); //Search result template

        } else {    
            get_template_part('template-parts/content', 'none');
        }
        ?>

    </div>
</div>   
<?php get_footer(); ?> 